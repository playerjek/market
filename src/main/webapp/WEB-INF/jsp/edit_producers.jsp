<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Producers</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/styles.css">
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
</head>
<body>
    <h1>Here you can manage Producers</h1>
    <table class="tableproducers" id="table_producers">
        <caption>Now next producers are avaiable</caption>
        <tr>
            <th>Producers</th>
            <th>Selector</th>
        </tr>
        <c:forEach items="${producers}" var="producer">
            <tr id="TR${producer.id}">
                <td><label><input id="prod${producer.id}" type="text" value="${producer.name}" name="producers" readonly></label></td>
                <td><label><input type="radio" name="producers_group" value="${producer.id}"></label></td>
            </tr>
        </c:forEach>
    </table>

    <button id="save">save</button>
    <button id="del">delete</button>

    <h2>Here you can create new producers</h2>
    <form action="${pageContext.request.contextPath}/add_producer">
        <label for="producername">Enter producer name:</label><input id="producername" name="producername" type="text" maxlength="25"/>
        <button type="submit" id="submitbutton" name="Create Producer">Create Producer</button>
    </form>
    <div class="nextpage">If you want back to laptops <a href="${pageContext.request.contextPath}/">click here</a></div>
    <script type='text/javascript'>
        $('input[name=producers_group]').click(function(){
            var prods = document.getElementsByName('producers');
            // loop through list of radio buttons
            for (var i=0, len=prods.length; i<len; i++) {
                prods[i].setAttribute('readonly', true);
            }
            $("#prod"+this.value).prop('readonly', false)
        });

        $("#save").click(function(){
            var radioVal = getRadioVal("producers_group");
            $.ajax({
                type: "post",
                url: "${pageContext.request.contextPath}/edit_producer",
                data: "{\"name\":\""+document.getElementById("prod"+radioVal).value+"\",\"id\":"+radioVal+"}",
                contentType: "application/json",
                success: function(textStatus) {
                    alert("data saved");
                    alert(textStatus);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                }
            })
        });

        $("#del").click(function(){
            var radioVal = getRadioVal("producers_group");
            if(confirm('Attention! If you delete this producer, it will delete all laptops of this producer. Are you sure?')) {
                $.ajax({
                    type: "post",
                    url: "${pageContext.request.contextPath}/delete_producer",
                    data: "{\"name\":\"" + document.getElementById("prod" + radioVal).value + "\",\"id\":" + radioVal + "}",
                    contentType: "application/json",
                    success: function (textStatus) {
                        alert(textStatus);
                        deleteProducer(radioVal);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        console.log(errorThrown);
                    }
                })
            }
        });

        function getRadioVal(name) {
            var val;
            var radios = document.getElementsByName(name);
            for (var i=0, len=radios.length; i<len; i++) {
                if ( radios[i].checked ) {
                    val = radios[i].value;
                    radios[i].checked=false;
                    break;
                }
            }
            return val;
        }

        function deleteProducer(id){
            var child = document.getElementById("TR" + id);
            child.parentNode.removeChild(child);
        }
    </script>
</body>
</html>
