package ua.luxoft.market.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ua.luxoft.market.dao.model.Laptop;
import ua.luxoft.market.dao.model.Producer;
import ua.luxoft.market.service.laptop.LaptopService;
import ua.luxoft.market.service.producer.ProducerService;

import java.util.List;

@Controller("/")
public class Market {

    @Autowired
    private LaptopService laptopService;

    @Autowired
    private ProducerService producerService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView present(){
        try {
            ModelAndView model = new ModelAndView("laptops");
            List<Laptop> laptops = laptopService.getLaptops();
            model.addObject("laptops", laptops);
            return model;
        } catch (Exception e){
            e.printStackTrace();
            return new ModelAndView("laptops");
        }
    }

    @RequestMapping(value = "/producers", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView showProducers(){
        try {
            ModelAndView model = new ModelAndView("edit_producers");
            List<Producer> producers = producerService.getProducers();
            model.addObject("producers", producers);
            return model;
        } catch (Exception e){
            e.printStackTrace();
            return present();
        }
    }

    @RequestMapping(value = "/add_producer", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView addProducer(@RequestParam String producername){
        try {
            producerService.createProducer(producername);
            return showProducers();
        } catch (Exception e){
            e.printStackTrace();
            return showProducers();
        }
    }

    @RequestMapping(value = "/edit_producer", method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody String editProducer(@RequestBody Producer producer){
        try {
            producerService.updateProducer(producer);
            return "success";
        } catch (Exception e){
            e.printStackTrace();
            return "error";
        }
    }

    @RequestMapping(value = "/delete_producer", method = {RequestMethod.GET, RequestMethod.POST})
    public @ResponseBody String deleteProducer(@RequestBody Producer producer){
        try {
            List<Laptop> laptops = laptopService.getLaptopsByProducer(producer);
            if (!laptops.isEmpty()){
                for (Laptop laptop: laptops)
                    laptopService.deleteLaptop(laptop);
            }
            producerService.deleteProducer(producer);
            return "success";
        } catch (Exception e){
            e.printStackTrace();
            return "error";
        }

    }
}
