package ua.luxoft.market.service.laptop;

import org.springframework.stereotype.Service;
import ua.luxoft.market.dao.model.Laptop;
import ua.luxoft.market.dao.model.Producer;

import java.util.List;

/**
 * Created by user on 08.09.15.
 */
@Service
public interface LaptopService {

    Laptop getLaptop(long id);
    Laptop getLaptopByModel(String model);
    List<Laptop> getLaptops();
    List<Laptop> getLaptopsByProducer(Producer producer);
    void deleteLaptop(Laptop laptop);
    void createLaptop(Laptop laptop);
}
